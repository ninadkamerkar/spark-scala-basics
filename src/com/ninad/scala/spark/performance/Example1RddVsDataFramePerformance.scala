package com.ninad.scala.spark.performance

import org.apache.spark.sql.SparkSession

object Example1RddVsDataFramePerformance {

  def main(args: Array[String]): Unit = {

    val sparkSession = SparkSession.builder()
      .appName("Rdd Vs DataFrame Performance")
      .master("local")
      .getOrCreate()

    val bankRdd = sparkSession.read
      .option("header", "true")
      .option("inferSchema", "true")
      .option("delimiter", ";")
      .csv("src/main/resources/bank.csv")
      
      val startTime = System.currentTimeMillis()
      
      bankRdd.where(bankRdd("marital") === "married").collect()
      
      val endTime = System.currentTimeMillis()
      
      println("===> "+(endTime - startTime))
      
      println("----------------------------------------------------")
      
      val startTime1 = System.currentTimeMillis()
      
      bankRdd.where(bankRdd("marital") === "married").collect()
      
      val endTime1 = System.currentTimeMillis()
      
      println("===> "+(endTime1 - startTime1))
  }
}