package com.ninad.scala.spark.dataframe

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.StructType
import org.apache.spark.sql.types.IntegerType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.TimestampType

object Example4ReadingJson {
  
  def main(args: Array[String]): Unit = {
    
    
    val sparkSession = SparkSession.builder()
    .appName("Creating DataFrame from JSON file")
    .master("local")
    .getOrCreate()
    
    //In Spark, for reading JSON file
    //1. Schema is inferred by default.
    //2. Column names are generated as per the json file
    val df = sparkSession.sqlContext.read
    //.option("timestampFormat", "yyyy/MM/dd HH:mm:ss")
    .json("src/main/resources/sample.json")
   
    df.printSchema()
    df.show()
    
//    println("------------------------------------------------------------------------------------------")
//    val schema = StructType(
//            StructField("name",StringType,true) ::
//            StructField("dob",TimestampType,true) :: Nil
//        )
//        
//    val df1 = sparkSession.sqlContext.read
//    .option("timestampFormat", "yyyy/MM/dd HH:mm:ss")
//    .schema(schema) 
//    .json("src/main/resources/sample.json")
//    
//    df1.printSchema()
//    df1.show()
  }
}