package com.ninad.scala.spark.dataframe

import scala.util.control.Breaks

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions
import org.apache.spark.sql.types.FloatType
import org.apache.spark.sql.types.IntegerType
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StructType

object SparkProblem {

  def main(args: Array[String]): Unit = {

    val sparkSession = SparkSession.builder()
      .appName("UK Weather Analysis")
      .master("local")
      .getOrCreate()

    import org.apache.spark.sql.functions._

    val stationList = List("aberporth", "armagh", "ballypatrick", "ballypatrick", "bradford", "braemar",
      "camborne", "cambridge", "cardiff", "chivenor", "cwmystwyth", "dunstaffnage", "durham", "eastbourne",
      "eskdalemuir", "heathrow", "hurn", "lerwick", "leuchars", "lowestoft", "manston", "nairn", "newtonrigg",
      "oxford", "paisley", "ringway", "rossonwye", "shawbury", "sheffield", "southampton", "stornoway", "suttonbonington",
      "tiree", "valley", "waddington", "whitby", "wickairport", "yeovilton")

    val allDF = collection.mutable.ArrayBuffer[DataFrame]()
    stationList.foreach(stationName => {
      //println("=========================> " + stationName)
      val df = getWeatherData(stationName, sparkSession)
      //df.show()
      allDF += df
    })

    //union all station data together
    val allStationData = allDF.reduce(_ union _)

    val groupDf = allStationData.groupBy(allStationData("Station")).count().sort(desc("count"))
    groupDf.show()
    println("Station by measure")
    
    val stationByRain = allStationData.groupBy(allStationData("Station")).max("Rain")
    stationByRain.show()
    println("Station by max rainfall")
    
    val stationBySunshine = allStationData.groupBy(allStationData("Station")).max("Sunshine")
    stationBySunshine.show()    
    println("Station by max Sunshine")

  }
  
//  def readWeatherFile(station: String,downloadMode: String): List[String] = {
//    
//    val textFileLines = scala.io.Source.fromURL("https://www.metoffice.gov.uk/pub/data/weather/uk/climate/stationdata/" + station + "data.txt").getLines().toList
//    return textFileLines
//  }  

  def getWeatherData(station: String, sparkSession: SparkSession): DataFrame = {
    //val textFile = sparkSession.sparkContext.textFile("D:/CODING/MY SPARK/Training Backup/SparkWorkspace/MavenScalaSpark2/" + station + "data.txt")

    //downloadFile(station)
    
    val textFileLines = scala.io.Source.fromURL("https://www.metoffice.gov.uk/pub/data/weather/uk/climate/stationdata/" + station + "data.txt").getLines().toList

    var lastHeader = 1

    val loop = new Breaks;

    loop.breakable {
      for (line <- textFileLines) {
        lastHeader += 1
        if (line.contains("yyyy")) {
          loop.break;
        }
      }
    }

    val weatherDataList = textFileLines
      .slice(lastHeader, textFileLines.size)
      .map(line1 => {
        line1
          .replace("---", "000")
          .replace("*", " ")
          .replace("#", " ")
          .replace("$", "")
          .replace("Provisional", "")
          .replace("Site closed", "")
          .replace("Change to Monckton Ave", "")
          .replace("||", "")
          .replace("Site Closed", "")
          .replace("all data from Whitby", "")
      }).filter(_ != "")
      .map(line => {
        //println(line)

        if (line.size > 42) {
          Row(station,
            line.substring(0, 7).trim().toInt,
            line.substring(35, 44).trim().toFloat,
            line.substring(44, line.size).trim().toFloat)

        } else {
          Row(station,
            line.substring(0, 7).trim().toInt,
            line.substring(35, line.size).trim().toFloat,
            "0".toFloat)
        }
      })

    //        Row(
    //          station,
    ////          line.substring(0, 4).trim().toInt,
    ////          line.substring(34, 42).trim().toFloat,
    ////          line.substring(42, line.size).trim().toFloat)
    //              line.substring(0, 7).trim().toInt,
    //              line.substring(35, 44).trim().toFloat,
    //              line.substring(44, line.size).trim().toFloat)

    val schema = StructType(
      StructField("Station", StringType, false) ::
        StructField("Year", IntegerType, false) ::
        StructField("Rain", FloatType, false) ::
        StructField("Sunshine", FloatType, false) :: Nil)


    return sparkSession.createDataFrame(sparkSession.sparkContext.parallelize(weatherDataList), schema)
  }

  def downloadFile(stationName: String) {
    try {
      val src = scala.io.Source.fromURL("https://www.metoffice.gov.uk/pub/data/weather/uk/climate/stationdata/" + stationName + "data.txt")
      val out = new java.io.FileWriter("D:/CODING/MY SPARK/Training Backup/SparkWorkspace/MavenScalaSpark/" + stationName + "data.txt")
      out.write(src.mkString)
      out.close
    } catch {
      case e: java.io.IOException => "error occured"
    }
  }  
}