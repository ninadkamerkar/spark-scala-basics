package com.ninad.scala.spark.dataframe

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.StructType
import org.apache.spark.sql.types.IntegerType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.TimestampType

object Example6ReadingParquet {
  
  def main(args: Array[String]): Unit = {
    
    
    val sparkSession = SparkSession.builder()
    .appName("Creating DataFrame from parquet file")
    .master("local")
    .getOrCreate()
    
    
    val df = sparkSession.sqlContext.read
    .parquet("src/main/resources/sample.parquet")
   
    df.printSchema()
    df.show()
  }
}