package com.ninad.scala.spark.dataframe

import org.apache.spark.sql.SparkSession

object Example5ReadingOrc {
  
  def main(args: Array[String]): Unit = {
   
    val sparkSession = SparkSession.builder()
    .appName("Creating DataFrame from ORC file")
    .master("local")
    .getOrCreate()
    
    
    val df = sparkSession.sqlContext.read
    .parquet("src/main/resources/sample.parquet")
   
    df.printSchema()
    df.show()
  }  
}