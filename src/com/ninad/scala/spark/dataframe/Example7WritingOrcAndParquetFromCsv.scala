package com.ninad.scala.spark.dataframe

import org.apache.spark.sql.SparkSession

object Example7WritingOrcAndParquetFromCsv {

  def main(args: Array[String]): Unit = {

    val sparkSession = SparkSession.builder()
      .appName("Writing CSV Dataframe to Orc and Parquet file")
      .master("local")
      .getOrCreate()

    val df = sparkSession.sqlContext.read
      .option("header", true)
      .option("inferSchema", true)
      .csv("src/main/resources/sample.csv")

    println("--------------------------------WRITING ORC--------------------------------")
    df.write
      .save("src/main/resources/parquet")

    println("--------------------------------WRITING ORC--------------------------------")
    df.write.format("orc")
      .save("src/main/resources/orc")
  }

}