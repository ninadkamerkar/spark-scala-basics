package com.ninad.scala.spark.dataframe

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.StructType
import org.apache.spark.sql.types.IntegerType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.TimestampType

object Example3 {
  
  def main(args: Array[String]): Unit = {
    
    
    val sparkSession = SparkSession.builder()
    .appName("Creating DataFrame from CSV file")
    .master("local")
    .getOrCreate()
    
    //In Spark, for reading csv file
    //1. No schema is inferred by default. All field are taken as String. 
    //   We need to enable inferSchema option in order to infer schema
    //2. Column names are generated randomly by default (_c0,_c1 etc). We need to provide schema 
    //   if we want to change the datatype and column names
    //3. First record is considered as data by default. We need to set header option if otherwise  
    
    val df = sparkSession.sqlContext.read
    .csv("src/main/resources/sample.csv")
   
    df.printSchema()
    df.show()

    println("------------------------------------------------------------------------------------------")
    val schema = StructType(
      StructField("name", StringType, true) ::
        StructField("age", IntegerType, true) ::
        StructField("dob", TimestampType, true) :: Nil)

    val df1 = sparkSession.sqlContext.read
      .option("header", "true") //Default is false 
      .option("delimiter", ",") // Default is comma
      .option("quote", "\"") //Default is double quotes
      .option("timestampFormat", "yyyy/MM/dd HH:mm:ss")
      .schema(schema)
      .csv("src/main/resources/sample.csv")

    df1.printSchema()
    df1.show()

    println("------------------------------------------------------------------------------------------")

    val df2 = sparkSession.sqlContext.read
      .option("header", "true")
      .option("inferSchema", "true") //Infer Schema option
      .option("timestampFormat", "yyyy/MM/dd HH:mm:ss")
      .csv("src/main/resources/sample.csv")

    println(df2.schema)
    df2.printSchema()
    df2.show()
  }
}