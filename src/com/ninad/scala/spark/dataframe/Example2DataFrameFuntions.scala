package com.ninad.scala.spark.dataframe

import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions
import org.apache.spark.sql.types.IntegerType
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StructType

object Example1DataFrameFuntions {
  
  def main(args: Array[String]): Unit = {
    
    val sparkSession = SparkSession.builder()
    .appName("Creating DataFrame from Rdd ")
    .master("local")
    .getOrCreate()
    
    //Creating RDD[int]
    val intRdd = sparkSession.sparkContext.parallelize(Array(Array(1,"B"),Array(2,"C"),Array(3,"A"),Array(4,"c")), 1) 

    //Converting RDD[int] to RDD[Row] 
    val rowRdd = intRdd.map(data => Row(data(0),data(1)))
    
    //Creating schema
    val schema = StructType(
      StructField("Sr_No",IntegerType,false) ::
      StructField("Name",StringType,false) :: Nil    
    )
    
    //Creating DF using RDD[Row] and Schema
    val df = sparkSession.createDataFrame(rowRdd,schema)

    //Print DataFrame schema
    df.printSchema()
    
    println("Schema = "+df.schema)
    
    println("\nDataFrame.describe")
    println(df.describe("Sr_No"))
    
    println("DataFrame.columns")
    df.columns.foreach(println)
    
    println("DataFrame.dtypes")
    df.dtypes.foreach(println)

    //DataFrame.count() = Action = Gets the counts
    println("DataFrame Count = "+df.count())

    //DataFrame.show() = Action = Displays all records in DataFrame in tabular form
    df.show()
    println("DataFrame.show()")    

    //DataFrame.collect() = Action = Fetches all records in DataFrame
    df.collect()
    println("DataFrame.collect()")

    println("DataFrame.explain()")
    df.explain(true)
    
    //DataFrame.head(num) = Transformation = Fetches top n records in DataFrame
    df.head(2)
    print("DataFrame.head(num)\n")    
    
    df.take(2)
    print("DataFrame.take(num)\n")

    //Selecting single columns
    df.select("name").show()
    print("DataFrame.select() = Selecting single column")
    df.select(df("name")).show()
    
    //Selecting multiple columns
    df.select("Name","Sr_No").show()
    print("DataFrame.select() = Selecting multiple column")

    //Filtering records using where condition
    df.select("name").where("Sr_No > 1").show()
    print("DataFrame.where()")
    
    val col1 = df("Sr_No").between(2, 4)
    df.where(col1).show()
    print("between()")

    //Filtering records using filter condition    
    df.filter("Sr_No > 2").show()
    print("DataFrame.filter()")
    
    //Another way of filtering
    df.filter(df.col("Sr_No").gt(2)).show()
    print("DataFrame.filter()")
    df.filter(df.col("Sr_No").equalTo(1)).show()
    print("DataFrame.filter()")
    
    //Drop column from DataFrame
    df.drop("Sr_No").show()
    print("DataFrame.drop() = Dropped Sr_No from DataFrame")
    
    //DataFrame to Rdd
    val dfToRdd = df.rdd
    println(dfToRdd)
    
    //ordering records
    import org.apache.spark.sql.functions._
    df.orderBy(desc("Sr_No")).show()
    print("DataFrame.orderBy() = Order by SR_NO descending\n")
    
    df.orderBy(desc("Sr_No"),desc("Name")).show()
    print("DataFrame.orderBy() = Order by SR_NO, NAME descending\n")
    
    df.sort(asc("Name")).show()
    print("DataFrame.sort() = Sort by NAME ascending\n")
    
    df.groupBy("Name").count().show()
  }
}