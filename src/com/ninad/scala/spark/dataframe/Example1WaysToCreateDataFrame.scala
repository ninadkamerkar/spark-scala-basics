package com.ninad.scala.spark.dataframe

import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.IntegerType
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StructType

object Example1WaysToCreateDataFrame {

  def main(args: Array[String]): Unit = {

    val sparkSession = SparkSession.builder()
    .appName("Creating DataFrame from Rdd")
    .master("local")
    .getOrCreate()

    //Option 1 
    val data = Seq(
      Row(8, "bat"),
      Row(64, "mouse"),
      Row(27, "horse"))

    val rowRdd = sparkSession.sparkContext.makeRDD(data, 1)

    val schema = StructType(
      List(
        StructField("number", IntegerType, true),
        StructField("word", StringType, true)))

    println("-------------------------- Option 1 = createDataFrame --------------------------")
    val df = sparkSession.createDataFrame(rowRdd, schema)
    df.show()

    println("-------------------------- Option 2 = makeRDD --------------------------")
    val df1 = sparkSession.sqlContext.applySchema(rowRdd, schema)
    df1.show()
  }
}