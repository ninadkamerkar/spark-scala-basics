package com.ninad.scala.spark.dataframe

import org.apache.spark.sql.SparkSession

object Example8DataFrameJoins {

  def main(args: Array[String]): Unit = {

    val sparkSession = SparkSession.builder()
      .appName("Creating DataFrame from ORC file")
      .master("local")
      .getOrCreate()

    val persons = Seq(
      ("Ninad", 35, 1),
      ("Shruti", 31, 2),
      ("Rutu", 4, 3))

    val cities = Seq(
      (1, "Dadar"),
      (2, "Dombivli"),
      (3, "Kalwa"))

    import sparkSession.implicits._

    val personDF = sparkSession.sparkContext.makeRDD(persons).toDF("personName","age","cityId")
    val cityDF = sparkSession.sparkContext.makeRDD(cities).toDF("cityId","cityName")
    
    personDF.join(cityDF,"cityId").show()
    
    personDF.registerTempTable("Person")
    sparkSession.sqlContext.sql("Select * from Person where personName = 'Ninad'").show()
    
    
    
    
    

  }

}