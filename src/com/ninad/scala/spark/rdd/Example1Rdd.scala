package com.ninad.scala.spark.rdd

import org.apache.spark.sql.SparkSession

object Example1Rdd {

  def main(args: Array[String]) : Unit = {

    val sparkSession = SparkSession.builder()
      .appName("First Spark RDD")
      .master("local")
      .getOrCreate()

      println("-------------------------- Scala Array -> Spark RDD --------------------------")
      val intArr = Array(1,2,3,4,5,6)
      println("Scala Array = "+intArr.mkString(" "))
      
      val intRdd = sparkSession.sparkContext.parallelize(intArr, 2)
      intRdd.setName("RDD 1")
      println(intRdd)
      intRdd.collect().foreach(println)
      
      println("-------------------------- Scala List -> Spark RDD --------------------------")
      val stringList = List("A","B","C","D","E","F")
      println("Scala List = "+stringList.mkString(" "))
      
      val stringRdd = sparkSession.sparkContext.parallelize(stringList)
      stringRdd.setName("RDD 2")
      println(stringRdd)
      stringRdd.collect().foreach(println)
      
      println("-------------------------- Scala List -> Spark RDD --------------------------")
      val anyList = List(1,"B",3,"D",4)
      
      val anyRdd = sparkSession.sparkContext.parallelize(anyList)
      anyRdd.setName("RDD 3")
      println(anyRdd)
      anyRdd.collect().foreach(println)
   }
}