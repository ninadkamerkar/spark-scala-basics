package com.ninad.scala.spark.rdd

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.Row

object Example3RddFunctions {
  def main(args: Array[String]): Unit = {
      val sparkSession = SparkSession.builder()
        .appName("Spark RDD Functions")
        .master("local")
        .getOrCreate()
        
      val sc = sparkSession.sparkContext
  
      val intArr = Array(1, 3, 2, 1, 5, 3, 5, 7, 1)
      println("Input Array = " + intArr.mkString(" "))
      val intRdd = sparkSession.sparkContext.parallelize(intArr, 1)
  
      println("RDD ID = " + intRdd.id)
      println("RDD Default Name = " + intRdd.name)
      intRdd.setName("My RDD")
      println("RDD Name = " + intRdd.name)
      println("Rdd Dependencies = " + intRdd.dependencies)
      println("RDD.count() = " + intRdd.count())
      println("Number of Partitions = " + intRdd.getNumPartitions)
      println("Debug String = "+intRdd.toDebugString)
  
      //FETCHING DATA
      //RDD.collect() = Action
      intRdd.collect()
  
      //RDD.first() = Action      
      println("RDD.first() = " + intRdd.first())
  
      //RDD.take() & foreach = Action
      println("RDD.take(num) ")
      intRdd.take(3).foreach(println)
  
      //RDD.distinct() = Action      
      println("RDD.distinct()")
      intRdd.distinct().foreach(println)
  
      //RDD.cache() = Transformation
      //cache is a lazy transformation. 
      //We need to fire action like collect/foreach on it to make it cache
      println("RDD.cache()")
      intRdd.cache().collect()
      println("Fetching RDD from cache")
      intRdd.foreach(println)
      
      //RDD.map() = Transformation
      println("RDD.map()")
      intRdd.map(x => (x ,1)).foreach(println)
      intRdd.map(x => x * 1).foreach(println)
      
      //RDD.filter() = Transformation
      println("RDD.filter()")
      intRdd.filter( x => x > 4).foreach(println)

      val stringRdd = sparkSession.sparkContext.parallelize(Array("Hello sir", "How are you?", "I am fine"))
      
      //RDD.flatMap() = Transformation
      println("RDD.flatMap()")
      stringRdd.flatMap(x => x.split(" ")).foreach(println)
      
      val rdd = sc.parallelize(Array("Joseph", "Jimmy", "Tina","Thomas", "James", "Cory","Christine", "Jackeline", "Juan"))
      
      //RDD.groupBy() = Transformation 
      println("RDD.groupBy()")
      rdd.groupBy(name => name.charAt(0)).foreach(println)
      
      //RDD.sortBy() = Transformation
      println("RDD.sortBy()")
      //Asc
      rdd.sortBy(name => name).foreach(println)
      //Desc
      rdd.sortBy(name => name,false).foreach(println)
      
      val rdd1 = sc.parallelize(Array(("D","Doll"),("A","Apple"),("C","Cat"),("B","Ball")))
      //Asc
      rdd1.sortByKey().foreach(println)
      //Desc
      rdd1.sortByKey(false).foreach(println)
      
      val rdd2 = sc.parallelize(Array(1,2,3,4,5,6))
      //Rdd.reduce() = Action
      println("Reduced (Addition) = "+rdd2.reduce(_ + _))
      println("Reduced (Addition) = "+rdd2.reduce((a,b) => (a+b)))
      
      //Rdd.mean() = Action
      println("Reduced (Mean) = "+rdd2.mean())
 
      //RDD.reduceByKey()
      val rdd3 = sc.parallelize(Array("a","b","a","b","c","a","d"))
      val mapRdd = rdd3.map(x => (x,1))
      
      mapRdd.reduceByKey(_+_).foreach(println)
      
      //Rdd1.union(Rdd2)
      val oneRdd = sc.parallelize(1  to 7,1)
      val twoRdd = sc.parallelize(4 to 10,1)
      println("Rdd.union()")
      oneRdd.union(twoRdd).foreach(println)

      //Rdd1.intersection(Rdd2)
      println("Rdd.intersection()")
      oneRdd.intersection(twoRdd).foreach(println)
      
      //Rdd1.subtract(Rdd2)
      println("Rdd.subtract()")
      oneRdd.subtract(twoRdd).foreach(println)   
      
      //Convert Rdd to row
      val rowRdd = oneRdd.map(element => Row(element))
      rowRdd.foreach(println)
  }
}