package com.ninad.scala.spark.rdd

import org.apache.spark.sql.SparkSession

object Example2DiffWaysToCreateRdd {

  def main(args: Array[String]) : Unit = {

    val sparkSession = SparkSession.builder()
      .appName("First Spark RDD")
      .master("local")
      .getOrCreate()

      println("-------------------------- Option 1 = parallelize --------------------------")
      val intRdd = sparkSession.sparkContext.parallelize(Array(1,2,3,4,5,6), 1)
      intRdd.collect().foreach(println)
      
      println("-------------------------- Option 2 = makeRDD --------------------------")
      val intRdd1 = sparkSession.sparkContext.makeRDD(Array(1,2,3,4,5,6), 1)
      intRdd1.collect().foreach(println)
  }
}