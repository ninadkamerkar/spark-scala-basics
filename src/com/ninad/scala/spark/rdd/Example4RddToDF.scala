package com.ninad.scala.spark.rdd

import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.IntegerType
import org.apache.spark.sql.types.StructType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StringType

object Example4RddToDF {
  
  def main(args: Array[String]): Unit = {
    
    val sparkSession = SparkSession.builder()
    .appName("Creating DataFrame from Rdd ")
    .master("local")
    .getOrCreate()
    
    //Creating RDD[int]
    val intRdd = sparkSession.sparkContext.parallelize(Array(Array(1,"Ninad"),Array(2,"Rutu"),Array(3,"Shruti")), 1) 

    //Converting RDD[int] to RDD[Row] 
    val rowRdd = intRdd.map(data => Row(data(0),data(1)))
    
    //Creating schema
    val schema = StructType(
      StructField("Sr_No",IntegerType,false) ::
      StructField("Name",StringType,false) :: Nil    
    )
    
    //Creating DF using RDD[Row] and Schema
    val df = sparkSession.createDataFrame(rowRdd,schema)

    //Print DataFrame schema
    df.printSchema()
    
    //DataFrame.show() = Action = Displays all records in DataFrame in tabular form
    println("\nDataFrame.show()")
    df.show()
  }
}